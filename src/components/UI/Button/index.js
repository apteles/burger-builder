import React from "react";
import styles from "./Button.module.css";

export default function Button({children,clicked, type}) {
  return <button className={[styles.Button, styles[type]].join(' ')} onClick={clicked}>{children}</button> ;
}
