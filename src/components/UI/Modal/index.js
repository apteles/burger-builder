import React from "react";
import styles from "./Modal.module.css";
import Aux from '../../../HOC/Aux'
import Backdrop from '../Backdrop'

export default function Modal({children, show, modalClosed}) {
  return ( 
        <Aux>
            <Backdrop show={show} clicked={modalClosed} />
             <div 
                className={styles.Modal}
                style={{ transform: show ? 'translateY(0)' : 'translateY(-100vh)', opacity: show ? '1' : '0' }}
                >
                 {children}
             </div>
        </Aux>  
       
    )
}
