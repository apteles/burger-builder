import React from "react";
import styles from "./OrderSummary.module.css";

import Aux from '../../../HOC/Aux'
import Button from '../../UI/Button'

export default function OrderSummary({ingredients, purchaseCancelled,purchaseContinued, totalPrice}) {
  
  const ingredientsSummary = Object.keys(ingredients).map(igKey => {
      return <li key={igKey}> <span style={{textTransform: 'capitalize'}}>{igKey}</span>: {ingredients[igKey]}</li>
  })
  
  return (
    <Aux>
        <h3>Your order</h3>
        <p>A delicious burger with the following ingredients:</p>
        <ul>
            {ingredientsSummary}
        </ul>
        <p><strong>Total Price: {totalPrice.toFixed(2)}</strong> </p>
        <p>Continue to Checkout?</p>
        <Button type='Danger' clicked={purchaseCancelled}>CANCEL</Button>
        <Button type='Success' clicked={purchaseContinued}>CONTINUE</Button>
    </Aux>
  )
}
