import React from "react";
import styles from "./BuildControls.module.css";
import BuildControl from "./BuildControl";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" }
];

export default function BuildControls({ currentPrice, ingredientAdded, ingredientRemove, disabled,purchasable,orderedHandler }) {
  return (
    <div className={styles.BuildControls}>
      <p>Current Price: <strong>{currentPrice.toFixed(2)}</strong> </p>
      {controls.map(ctrl => (
        <BuildControl
          addHandler={_ => ingredientAdded(ctrl.type)}
          removeHandler={_ => ingredientRemove(ctrl.type)}
          key={ctrl.label}
          label={ctrl.label}
          disabled={disabled[ctrl.type]}
        />
      ))}
      <button onClick={orderedHandler} disabled={purchasable} className={styles.OrderButton}>ORDER NOW</button>
    </div>
  );
}
