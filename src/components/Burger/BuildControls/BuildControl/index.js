import React from "react";
import PropTypes from "prop-types";
import styles from "./BuildControl.module.css";

function BuildControl({ label, addHandler, removeHandler,disabled }) {
  return (
    <div className={styles.BuildControl}>
      <div className={styles.Label}>{label}</div>
      <button onClick={removeHandler} className={styles.Less} disabled={disabled}>
        Less
      </button>
      <button onClick={addHandler} className={styles.More}>
        More
      </button>
    </div>
  );
}
BuildControl.propTypes = {
  label: PropTypes.string.isRequired,
  addHandler: PropTypes.func.isRequired,
  removeHandler: PropTypes.func.isRequired
};

export default BuildControl;
