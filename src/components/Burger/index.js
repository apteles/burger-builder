import React from "react";

import styles from "./Burger.module.css";
import Ingridient from "./Ingridient";

export default function Burger({ ingredients }) {
  let transformedIngredients = Object.keys(ingredients)
    .map(igKey => {
      return [...Array(ingredients[igKey])].map((_, i) => {
        return <Ingridient key={igKey + i} type={igKey} />;
      });
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []);

  if (transformedIngredients.length === 0) {
    transformedIngredients = <p>Please start adding ingredients!</p>;
  }
  return (
    <div className={styles.Burger}>
      <Ingridient type="bread-top" />
      {transformedIngredients}
      <Ingridient type="bread-bottom" />
    </div>
  );
}
