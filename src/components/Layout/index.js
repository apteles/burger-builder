import React from "react";
import AuxHOC from "../../HOC/Aux";
import Toolbar from "../Navigation/Toolbar";
import styles from "./Layout.module.css";

function Layout({ children }) {
  return (
    <AuxHOC>
      
      <Toolbar />
      <main className={styles.Content}>{children}</main>
    </AuxHOC>
  );
}

export default Layout;
