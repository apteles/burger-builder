import React, { Component } from "react";
import Aux from "../../HOC/Aux";
import Burger from "../../components/Burger";
import BuildControls from "../../components/Burger/BuildControls";
import OrderSummary from "../../components/Burger/OrderSummary";
import Modal from "../../components/UI/Modal";

const INGREDIENT_PRICES = {
  salad: 0.5,
  cheese: 0.4,
  meat: 0.4,
  bacon: 0.7
};
export default class BurgerBuilder extends Component {
  state = {
    ingredients: {
      salad: 0,
      bacon: 0,
      cheese: 0,
      meat: 0
    },
    totalPrice: 0,
    purchasable: false,
    purchasing: false
  };

  updatePurchaseState(ingridients){

    const sum = Object.keys(ingridients)
                  .map(igKey =>  ingridients[igKey])
                  .reduce((sum,el) => sum + el, 0)
                  this.setState({purchasable: sum > 0})
  }

  purchaseHandler = () =>{
    this.setState({purchasing: true})
  }

  purchaseCancelHandler = () =>{
    this.setState({purchasing: false})
  }

  purchaseContiueHandler = () =>{
    alert('You continue!');
  }


  addIngredientHandle = type => {
    const oldCount = this.state.ingredients[type];
    const updatedCount = oldCount + 1;
    const updatedIngredients = {
      ...this.state.ingredients
    };
    updatedIngredients[type] = updatedCount;
    const priceAddition = INGREDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice + priceAddition;
    this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
    this.updatePurchaseState(updatedIngredients)
  };

  removeIngredientHandle = type => {
    const oldCount = this.state.ingredients[type];
    if(oldCount <= 0){
      return;
    }
    const updatedCount = oldCount - 1;
    const updatedIngredients = {
      ...this.state.ingredients
    };
    updatedIngredients[type] = updatedCount;
    const priceDeduction = INGREDIENT_PRICES[type];
    const oldPrice = this.state.totalPrice;
    const newPrice = oldPrice - priceDeduction;
    this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
    this.updatePurchaseState(updatedIngredients)
  };

  render() {
    const { ingredients, totalPrice, purchasable, purchasing } = this.state;
    const disabledInfo = {...ingredients}

    for(let key in ingredients){
      disabledInfo[key] = disabledInfo[key] <= 0
    }

    return (
      <Aux>

        <Modal show={purchasing} modalClosed={this.purchaseCancelHandler}>
        <OrderSummary totalPrice={totalPrice}  ingredients={ingredients} purchaseCancelled={this.purchaseCancelHandler} purchaseContinued={this.purchaseContiueHandler} />
        </Modal>
       
        <Burger ingredients={ingredients} />
        <BuildControls 
          currentPrice={totalPrice} 
          ingredientAdded={this.addIngredientHandle} 
          ingredientRemove={this.removeIngredientHandle} 
          disabled={disabledInfo}
          purchasable={!purchasable}
          orderedHandler={this.purchaseHandler}
          />
      </Aux>
    );
  }
}
